
def count_even_numbers(numbers):
    count = 0
    for num in numbers:
        if num % 2 == 0:
            count += 1
    return count

input_str = input("Unesi intedzere odvojene zarezom:")
input_list = input_str.split(',')
numbers = [int(num) for num in input_list]


even_count = count_even_numbers(numbers)

print("Broj parnih intedera:", even_count)
