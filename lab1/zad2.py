 
def centered_average(numbers):
  
    numbers.sort()
    
  
    numbers = numbers[1:-1]
    
   
    total = sum(numbers)
    
  
    avg = total // len(numbers)
    
    return avg


input_str = input("Unesi intedere odvojene zarezom: ")
input_list = input_str.split(',')
numbers = [int(num) for num in input_list]


if len(numbers) < 3:
    print("Lista mora sadrzavat 3 intedera.")
else:
    centered_avg = centered_average(numbers)
    print("Centrirana sredina:", centered_avg)
