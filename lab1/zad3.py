
def has_2_to_2(numbers):
    for i in range(len(numbers) - 1):
        if numbers[i] == 2 and numbers[i + 1] == 2:
            return True
    return False

input_str = input("Unesi intedere odvojene zarezom: ")
input_list = input_str.split(',')
numbers = [int(num) for num in input_list]


if has_adjacent_2(numbers):
    print("Lista sadrzi dvojku do dvojke.")
else:
    print("Lista ne sadrzi dvojku do dvojke")
