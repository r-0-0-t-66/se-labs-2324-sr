
def count_char(string):
    uppercase_count = 0
    lowercase_count = 0
    number_count = 0

    for char in string:
        if char.isupper():
            uppercase_count += 1
        elif char.islower():
            lowercase_count += 1
        elif char.isnumeric():
            number_count += 1

    return (uppercase_count, lowercase_count, number_count)


input_string = input("Unesi string: ")


result = count_characters(input_string)



print("Uppercase:", result[0])
print("Lowercase:", result[1])
print("Brojevi:", result[2])
