from query_handler_base import QueryHandlerBase
import requests
import json
import random


class ChuckNorrisHandler(QueryHandlerBase):
    def can_process_query(self, query):
        if "chuck" in query:
            return True
        return False

    def process(self, query):

        try:
            result = self.call_api()
            joke = result["value"]
            self.ui.say(f"{joke}")

        except:

            self.ui.say("Try something else!")

    def call_api(self):
        url = "https://matchilling-chuck-norris-jokes-v1.p.rapidapi.com/jokes/random"

        headers = {
            "accept": "application/json",
            "X-RapidAPI-Key": "417fca4d7emsh7bfbdaab59476a1p199233jsn9f50ed37fc9c",
            "X-RapidAPI-Host": "matchilling-chuck-norris-jokes-v1.p.rapidapi.com"
        }

        response = requests.get(url, headers=headers)
        return json.loads(response.text)
