from query_handler_base import QueryHandlerBase
import random
import requests
import json


class WeatherHandler(QueryHandlerBase):
    def can_process_query(self, query):
        if "weather" in query:
            return True
        return False

    def process(self, query):
        input = query.split()
        city = input[1]

        try:
            result = self.call_api(city)
            txt = result["current"]["temp_c"]
            self.ui.say(txt)
        except Exception as e:
            self.ui.say("Oh no! There was an error.")
            self.ui.say("Try something else!")

    def call_api(self, city):
        url = "https://weatherapi-com.p.rapidapi.com/current.json"

        querystring = {"q": city}

        headers = {
            "X-RapidAPI-Key": "417fca4d7emsh7bfbdaab59476a1p199233jsn9f50ed37fc9c",
            "X-RapidAPI-Host": "weatherapi-com.p.rapidapi.com"

        }

        response = requests.get(url, headers=headers, params=querystring)

        return json.loads(response.text)
